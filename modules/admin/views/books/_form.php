<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\models\Books */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="books-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data',
        ]
    ]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => TRUE]) ?>

    <?= $form->field($model, 'author')->textInput(['maxlength' => TRUE]) ?>

    <?= $form->field($model, 'image')->fileInput() ?>

    <div class="form-group">
        <div class="cold-md-12">
            <p>
                <?php $image = $model->getImage();
                $sizes = $image->getSizesWhen('x200');
                echo '<img width="'.$sizes['width'].'" height="'.$sizes['height'].'" src="'.$image->getUrl('x200').'" />';
                ?>
            </p>
        </div>
    </div>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'publish_date')
        ->widget(\yii\jui\DatePicker::classname(), [
            'language' => 'ru',
            'dateFormat' => 'yyyy-MM-dd',
        ]) ?>
    <?= $form->field($model, 'count')->checkbox() ?>


    <?php //= $form->field($model, 'created_at')->textInput() ?>

    <?php //= $form->field($model, 'updated_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
