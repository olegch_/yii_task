<?php

use yii\db\Migration;

/**
 * Handles the creation for table `books`.
 */
class m160816_040627_create_books_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('books', [
            //'id' => Schema::TYPE_PK,
            'id' => $this->primaryKey(),
            //'title' => Schema::TYPE_STRING . ' NOT NULL',
            'title' => $this->string(50)->notNull(),
            //'author' => Schema::TYPE_STRING,
            'author' => $this->string(255),
            //'image' => Schema::TYPE_STRING,
            'image' => $this->string(255),
            //'description' => Schema::TYPE_TEXT,
            'description' => $this->text(),
            //'publish_date' => Schema::TYPE_DATE,
            'publish_date' => $this->date(),
            //'count' => Schema::TYPE_INTEGER,
            'count' => $this->integer(11),
            //'timestamp' => Schema::TYPE_TIMESTAMP
            'created_at' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('books');
    }
}
