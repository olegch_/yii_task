<?php

namespace app\controllers;

use Yii;
use app\models\Books;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * BooksController implements the CRUD actions for Books model.
 */
class BooksController extends Controller
{
    /**
     * Lists all Books models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = Books::find()->all();
        //return $this->render('index', ['model' => $model]);
        return $this->render('index', [
          'model' => $model,
        ]);
    }

    /**
     * Displays a single Books model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $model->count = ($model->count  == 1) ? 'есть' : 'нет';

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the Books model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Books the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Books::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
