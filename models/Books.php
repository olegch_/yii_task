<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Books".
 *
 * @property integer $id
 * @property string $title
 * @property string $author
 * @property string $image
 * @property string $description
 * @property string $publish_date
 * @property integer $count
 * @property integer $created_at
 * @property integer $updated_at
 */
class Books extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Books';
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'image' => [
                'class' => 'rico\yii2images\behaviors\ImageBehave',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'created_at', 'updated_at'], 'required'],
            [['description'], 'string'],
            [['publish_date'], 'safe'],
            [['count', 'created_at', 'updated_at'], 'integer'],
            [['title'], 'string', 'max' => 50],
            [['author', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'author' => 'Автор',
            'image' => 'Обложка',
            'description' => 'Описание',
            'publish_date' => 'Дата публикации',
            'count' => 'Наличие',
            'created_at' => 'Создано',
            'updated_at' => 'Обновлено',
        ];
    }
}
