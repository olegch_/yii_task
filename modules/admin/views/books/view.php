<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Books */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Books', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="books-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', [
            'update',
            'id' => $model->id
        ], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?php
    $image = $model->getImage();
    $sizes = $image->getSizesWhen('x500');

    echo newerton\fancybox\FancyBox::widget([
        'target' => 'a[rel=fancybox]',
        'helpers' => TRUE,
        'mouse' => TRUE,
        'config' => [
            'maxWidth' => '90%',
            'maxHeight' => '90%',
            'playSpeed' => 3000,
            'padding' => 0,
            'fitToView' => FALSE,
            'width' => '70%',
            'height' => '70%',
            'autoSize' => FALSE,
            'closeClick' => FALSE,
            'openEffect' => 'elastic',
            'closeEffect' => 'elastic',
            'prevEffect' => 'elastic',
            'nextEffect' => 'elastic',
            'closeBtn' => FALSE,
            'openOpacity' => TRUE,
            'helpers' => [
                'title' => ['type' => 'float'],
                'buttons' => [],
                'thumbs' => ['height' => 200],
                'overlay' => [
                    'css' => [
                        'background' => 'rgba(0, 0, 0, 0.8)'
                    ]
                ]
            ],
        ]
    ]);

    $image_output = Html::a(Html::img($image->getUrl('x200'), ['height' => 200]), $image->getUrl('x500'), ['rel' => 'fancybox']);

    ?>
    <div class="books-view">

        <h1><?= Html::encode($this->title) ?></h1>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'title',
                'author',
                [
                    'label' => 'Обложка',
                    'format' => 'raw',
                    'value' => $image_output,
                ],
                'description:ntext',
                'publish_date:datetime',
                'count',
                'created_at:datetime',
                'updated_at:datetime',
            ],
        ]) ?>

    </div>
