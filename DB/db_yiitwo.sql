-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 16, 2016 at 10:40 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_yiitwo`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth_assignment`
--

CREATE TABLE IF NOT EXISTS `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('admin', '1', 1471362303),
('admin', '2', 1471379513);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item`
--

CREATE TABLE IF NOT EXISTS `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('/admin/*', 2, NULL, NULL, NULL, 1471361969, 1471361969),
('/books/*', 2, NULL, NULL, NULL, 1471361978, 1471361978),
('/rbac/*', 2, NULL, NULL, NULL, 1471361974, 1471361974),
('/site/*', 2, NULL, NULL, NULL, 1471361986, 1471361986),
('admin', 1, 'доступ к админ панели', NULL, NULL, 1471362254, 1471362254),
('adminAccess', 2, 'доступ в админку', NULL, NULL, 1471362097, 1471362112),
('BooksAccess', 2, 'доступ к чтению Книг', NULL, NULL, 1471362052, 1471362052);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item_child`
--

CREATE TABLE IF NOT EXISTS `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_item_child`
--

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('adminAccess', '/admin/*'),
('BooksAccess', '/books/*'),
('adminAccess', '/rbac/*'),
('BooksAccess', '/site/*'),
('admin', 'adminAccess'),
('admin', 'BooksAccess');

-- --------------------------------------------------------

--
-- Table structure for table `auth_rule`
--

CREATE TABLE IF NOT EXISTS `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE IF NOT EXISTS `books` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `author` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `description` text,
  `publish_date` date DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `title`, `author`, `image`, `description`, `publish_date`, `count`, `created_at`, `updated_at`) VALUES
(1, 'Оружие возмездия', 'Пелевин', '', 'Lorem ipsum dolor sit amet, eu per wisi omnium integre, in tale cibo vim. Sea posse tamquam interesset no, appareat gloriatur id cum, latine gubergren in vix. Et sit lorem detraxit, illud erant oporteat te sed. Ea harum latine sententiae usu, vis vide autem alterum no. No aeque solet ancillae mea, libris bonorum vix cu, ea docendi explicari pertinacia has.\r\n\r\nId eam ignota maluisset. Ei sea reformidans interpretaris, legimus pertinax no usu. Ius modus epicurei an. In usu tritani comprehensam, ea nibh facer dissentias usu.\r\n\r\nCommune molestie philosophia te quo, labore prodesset ad eam. Mei id suas congue moderatius, sit ea quidam oporteat menandri, qui dico iusto nonumes ei. Eos dicta viris definitionem no, per harum eirmod iuvaret et. Partem populo no has, ut his ipsum vocibus offendit, te enim erroribus molestiae pri. Ex tale ancillae quo, summo fastidii pertinacia vim et, persius ancillae no nam.\r\n\r\nEx facilisis laboramus nam, vel ex tritani deterruisset. Tantas minimum philosophia ei sed, eos meis vivendum ex. Diam denique te sit, nam ancillae molestiae maiestatis ad. Quo utamur scripserit in, te mei homero suavitate. Mei aliquip delectus corrumpit te. Minim error luptatum ea qui, an vel quodsi quaeque, eum iudico vivendo deleniti ne.\r\n\r\nUsu suas dolor dicunt et, per in autem iuvaret. Sumo nonumes accusamus sed no. Esse causae an per. Habeo porro ei cum, zril intellegat eos te. Sit oblique adolescens id.', '1900-08-13', 1, 1471327153, 1471379996),
(2, 'Евгений Онегин', 'Пушкин', '', 'Lorem ipsum dolor sit amet, eu per wisi omnium integre, in tale cibo vim. Sea posse tamquam interesset no, appareat gloriatur id cum, latine gubergren in vix. Et sit lorem detraxit, illud erant oporteat te sed. Ea harum latine sententiae usu, vis vide autem alterum no. No aeque solet ancillae mea, libris bonorum vix cu, ea docendi explicari pertinacia has.\r\n\r\nId eam ignota maluisset. Ei sea reformidans interpretaris, legimus pertinax no usu. Ius modus epicurei an. In usu tritani comprehensam, ea nibh facer dissentias usu.\r\n\r\nCommune molestie philosophia te quo, labore prodesset ad eam. Mei id suas congue moderatius, sit ea quidam oporteat menandri, qui dico iusto nonumes ei. Eos dicta viris definitionem no, per harum eirmod iuvaret et. Partem populo no has, ut his ipsum vocibus offendit, te enim erroribus molestiae pri. Ex tale ancillae quo, summo fastidii pertinacia vim et, persius ancillae no nam.\r\n\r\nEx facilisis laboramus nam, vel ex tritani deterruisset. Tantas minimum philosophia ei sed, eos meis vivendum ex. Diam denique te sit, nam ancillae molestiae maiestatis ad. Quo utamur scripserit in, te mei homero suavitate. Mei aliquip delectus corrumpit te. Minim error luptatum ea qui, an vel quodsi quaeque, eum iudico vivendo deleniti ne.\r\n\r\nUsu suas dolor dicunt et, per in autem iuvaret. Sumo nonumes accusamus sed no. Esse causae an per. Habeo porro ei cum, zril intellegat eos te. Sit oblique adolescens id.', '1823-04-04', 0, 1471327187, 1471379773),
(3, 'Мертвые души', 'Гоголь', '', '«Мёртвые ду́ши» — произведение Николая Васильевича Гоголя, жанр которого сам автор обозначил как поэма. Изначально задумано как трёхтомное произведение. Первый том был издан в 1842 году. Практически готовый второй том уничтожен писателем, но сохранилось несколько глав в черновиках. Третий том был задуман и не начат, о нём остались только отдельные сведения.', '1842-02-27', 1, 1471368777, 1471379755),
(4, 'Война и Мир', 'Толстой', '', 'Замысел эпопеи формировался задолго до начала работы над тем текстом, который известен под названием «Война и мир». В наброске предисловия к «Войне и миру» Толстой писал, что в 1856 г. начал писать повесть, «герой которой должен был быть декабрист, возвращающийся с семейством в Россию. Невольно от настоящего я перешёл к 1825 году… Но и в 1825 году герой мой был уже возмужалым, семейным человеком. Чтобы понять его, мне нужно было перенестись к его молодости, и молодость его совпала с … эпохой 1812 года… Ежели причина нашего торжества была не случайна, но лежала в сущности характера русского народа и войска, то характер этот должен был выразиться ещё ярче в эпоху неудач и поражений…» Так Толстой постепенно пришёл к необходимости начать повествование с 1805 года.\r\n\r\n\r\nКорпус Бородинского музея, занятый экспозицией, посвящённой роману\r\n\r\nПамятный знак на усадьбе, которая послужила прообразом дома Ростовых в романе Л. Н. Толстого «Война и мир». Москва, Поварская улица, 55\r\nК работе над повестью Толстой возвращался несколько раз. В начале 1861 года он читал главы из романа «Декабристы», написанные в ноябре 1860 — начале 1861 года, Тургеневу и сообщал о работе над романом Герцену.[1] Однако работа несколько раз откладывалась, пока в 1863—1869 гг. не был написан роман «Война и мир». Некоторое время роман-эпопея воспринимался Толстым как часть повествования, которое должно было закончиться возвращением Пьера и Наташи из сибирской ссылки в 1856 году (именно об этом идёт речь в 3-х сохранившихся главах романа «Декабристы»). Попытки работы над этим замыслом предпринимались Толстым последний раз в конце 1870-х годов, после окончания «Анны Карениной».\r\n\r\nРоман «Война и мир» имел большой успех. Отрывок из романа под названием «1805 год» появился в «Русском вестнике» в 1865 году. В 1868 году вышли три его части, за которыми вскоре последовали остальные две (всего четыре тома)[2].\r\n\r\nПризнанный критикой всего мира величайшим эпическим произведением новой европейской литературы, «Война и мир» поражает уже с чисто технической точки зрения размерами своего беллетристического полотна. Только в живописи можно найти некоторую параллель в огромных картинах Паоло Веронезе в венецианском Дворце дожей, где тоже сотни лиц выписаны с удивительною отчётливостью и индивидуальным выражением[3]. В романе Толстого представлены все классы общества, от императоров и королей до последнего солдата, все возрасты, все темпераменты и на пространстве целого царствования Александра I[3]. Что ещё более возвышает его достоинство как эпоса — это данная им психология русского народа. С поражающим проникновением изобразил Толстой настроения толпы, как высокие, так и самые низменные и зверские (например, в знаменитой сцене убийства Верещагина).', '1805-01-10', 1, 1471368876, 1471379729);

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE IF NOT EXISTS `image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filePath` varchar(400) NOT NULL,
  `itemId` int(20) NOT NULL,
  `isMain` int(1) DEFAULT NULL,
  `modelName` varchar(150) NOT NULL,
  `urlAlias` varchar(400) NOT NULL,
  `name` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `image`
--

INSERT INTO `image` (`id`, `filePath`, `itemId`, `isMain`, `modelName`, `urlAlias`, `name`) VALUES
(5, 'Books/Books4/c1e74c.jpg', 4, 1, 'Books', '3215b336da-1', ''),
(6, 'Books/Books3/788872.jpg', 3, 1, 'Books', '10b75f7ea0-1', ''),
(7, 'Books/Books2/574e99.jpg', 2, 1, 'Books', 'a34b860682-1', ''),
(11, 'Books/Books1/356dbf.jpg', 1, 1, 'Books', '43f970aa49-1', '');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `parent` int(11) DEFAULT NULL,
  `route` varchar(255) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `data` blob,
  PRIMARY KEY (`id`),
  KEY `parent` (`parent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1471321583),
('m140506_102106_rbac_init', 1471360159),
('m140602_111327_create_menu_table', 1471360142),
('m140622_111540_create_image_table', 1471369874),
('m140622_111545_add_name_to_image_table', 1471369874),
('m160312_050000_create_user', 1471360143),
('m160816_040627_create_books_table', 1471326837);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Ey1ME6Bd_WZSTxX93ob2EW_DBRkYdH9P', '$2y$13$3nLPjXvczHtrNBoshuvLkeG2unA5KLkh.M1fjHnz0unkwUwnmPCQa', NULL, 'hojo@list.ru', 10, 1471361855, 1471361855),
(2, 'root', 'v2w4sEoIlH70VMOHwj8aF0qD7zlmJbqG', '$2y$13$/bCrcGG.bYz/NXDhDeLVeuZk1HqiLvYk18Zfns88g9aVdXARmkshK', NULL, 'root@yii.com', 10, 1471379497, 1471379497);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu`
--
ALTER TABLE `menu`
  ADD CONSTRAINT `menu_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `menu` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
