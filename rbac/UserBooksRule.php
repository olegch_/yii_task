<?php
/**
 * Created by PhpStorm.
 * User: hojo
 * Date: 16.08.2016
 * Time: 12:46
 */

namespace app\rbac;

use yii\rbac\Rule;
use yii\rbac\Item;

class BooksRule extends Rule
{
  public $name = 'isAuthUser';

  /**
   * @param string|integer $user   the user ID.
   * @param Item           $item   the role or permission that this rule is associated with
   * @param array          $params parameters passed to ManagerInterface::checkAccess().
   *
   * @return boolean a value indicating whether the rule permits the role or permission it is associated with.
   */
  public function execute($user, $item, $params)
  {
    if (\Yii::$app->user->identity->group == 'admin') {
      return true;
    }else{
      echo "123";
      return false;
    }
   // return isset($params['profileId']) ? \Yii::$app->user->id == $params['profileId'] : false;
  }
}