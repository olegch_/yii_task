<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BooksSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Books';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="books-index">


    <?php if (count($model)): ?>
        <?php foreach ($model as $item): ?>
            <div class="well">
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <div class="caption">
                            <h3><?= $item->title ?></h3>
                            <p>
                            <?php
                            $image = $item->getImage();
                            $sizes = $image->getSizesWhen('x500');

                            echo newerton\fancybox\FancyBox::widget([
                                'target' => 'a[rel=fancybox_'.$item->id.']',
                                'helpers' => true,
                                'mouse' => true,
                                'config' => [
                                    'maxWidth' => '90%',
                                    'maxHeight' => '90%',
                                    'playSpeed' => 3000,
                                    'padding' => 0,
                                    'fitToView' => false,
                                    'width' => '70%',
                                    'height' => '70%',
                                    'autoSize' => false,
                                    'closeClick' => false,
                                    'openEffect' => 'elastic',
                                    'closeEffect' => 'elastic',
                                    'prevEffect' => 'elastic',
                                    'nextEffect' => 'elastic',
                                    'closeBtn' => false,
                                    'openOpacity' => true,
                                    'helpers' => [
                                        'title' => ['type' => 'float'],
                                        'buttons' => [],
                                        'thumbs' => ['height' => 200],
                                        'overlay' => [
                                            'css' => [
                                                'background' => 'rgba(0, 0, 0, 0.8)'
                                            ]
                                        ]
                                    ],
                                ]
                            ]);

                            echo Html::a(Html::img($image->getUrl('x200'), ['height' => 200]), $image->getUrl('x500'), ['rel' => 'fancybox_'.$item->id]);
                            ?>
                            </p>
                            <p><?= $item->author ?></p>
                            <p><?= $item->description ?></p>
                            <p><?= $item->publish_date ?></p>
                            <p><a href="<?= Url::toRoute([
                                    '/books/view',
                                    'id' => $item->id
                                ]); ?>" class="btn btn-primary"
                                  role="button">Просмотр</a></p>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach ?>
    <?php endif ?>
</div>
